package com.gama.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.gama.domain.model.Repository
import com.gama.domain.exception.DefaultException
import com.gama.domain.usecase.RepositoryUseCase
import com.gama.presentation.viewmodel.RepositoryViewModel
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.anyMap
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class RepositoryViewModelTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var useCase: RepositoryUseCase

    @InjectMocks
    lateinit var viewModel: RepositoryViewModel

    @Mock
    lateinit var observerRepositories: Observer<List<Repository>>

    @Mock
    lateinit var observerError: Observer<DefaultException>

    @Before
    fun setup() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `Test getRepositories() success`() {
        // PREPARE
        val subjectDelay = PublishSubject.create<MutableList<Repository>>()
        val repositories = listOf<Repository>()

        `when`(useCase.getRepositories(anyMap())).thenReturn(
            Single.just(repositories).delaySubscription(subjectDelay)
        )

        viewModel.repositories.observeForever(observerRepositories)

        // ACTION
        viewModel.fetchRepositories()

        // TEST
        assertTrue(viewModel.showLoader.get())
        subjectDelay.onComplete()
        assertFalse(viewModel.showLoader.get())

        verify(observerRepositories).onChanged(repositories)
    }

    @Test
    fun `Test getRepositories() error`() {
        // PREPARE
        val exception = DefaultException()

        `when`(useCase.getRepositories(anyMap())).thenReturn(
            Single.error(exception)
        )

        viewModel.error.observeForever(observerError)

        // ACTION
        viewModel.fetchRepositories()

        verify(observerError).onChanged(exception)
    }
}