package com.gama.presentation.ui.activity

import android.os.Bundle
import com.gama.presentation.R
import com.gama.presentation.adapter.RepositoryAdapter
import com.gama.presentation.base.BaseActivity
import com.gama.presentation.databinding.ActivityRepositoryBinding
import com.gama.presentation.extensions.observeNotNull
import com.gama.presentation.viewmodel.RepositoryViewModel

class RepositoryActivity : BaseActivity<ActivityRepositoryBinding, RepositoryViewModel>() {

    override fun getLayoutRes() = R.layout.activity_repository

    override fun getViewModelByClass() = RepositoryViewModel::class

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.vm = viewModel
        setupObservers()

        viewModel.fetchRepositories()
    }

    private fun setupObservers() {
        viewModel.repositories.observeNotNull(this) {
            val adapter = RepositoryAdapter(it)
            binding.rclRepository.adapter = adapter
        }

        viewModel.error.observeNotNull(this) {
        }
    }
}
