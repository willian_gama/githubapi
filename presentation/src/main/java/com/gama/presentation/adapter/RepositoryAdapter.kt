package com.gama.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gama.domain.model.Repository
import com.gama.presentation.R
import com.gama.presentation.base.BaseRecyclerViewAdapter
import com.gama.presentation.databinding.HolderRepositoryBinding
import com.gama.presentation.viewmodel.RepositoryHolderViewModel

class RepositoryAdapter(private val repositories: List<Repository>) :
    BaseRecyclerViewAdapter<HolderRepositoryBinding, RepositoryAdapter.ViewHolder>() {

    override fun getItemCount() = repositories.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(dataBinding(parent, R.layout.holder_repository))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(repositories[position])

    inner class ViewHolder(private val binding: HolderRepositoryBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(repository: Repository) {
            binding.vm = RepositoryHolderViewModel(repository)
            binding.executePendingBindings()
        }
    }
}