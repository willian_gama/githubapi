package com.gama.presentation.di

import com.gama.presentation.viewmodel.RepositoryViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {
    viewModel { RepositoryViewModel(get()) }
}