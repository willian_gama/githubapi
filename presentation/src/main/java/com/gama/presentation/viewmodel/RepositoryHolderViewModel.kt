package com.gama.presentation.viewmodel

import com.gama.domain.model.Repository
import com.gama.presentation.base.BaseViewModel

class RepositoryHolderViewModel(private val repository: Repository?) : BaseViewModel() {
    fun getName() = repository?.name

    fun getDescription() = repository?.description

    fun getAvatarUrl() = repository?.owner?.avatarUrl
}