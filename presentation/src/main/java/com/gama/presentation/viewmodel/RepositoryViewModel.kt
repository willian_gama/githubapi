package com.gama.presentation.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import com.gama.domain.model.Repository
import com.gama.domain.exception.DefaultException
import com.gama.domain.usecase.RepositoryUseCase
import com.gama.presentation.R
import com.gama.presentation.base.BaseViewModel
import com.gama.presentation.utils.SingleLiveData

class RepositoryViewModel(private val useCase: RepositoryUseCase) : BaseViewModel() {
    val showLoader = ObservableBoolean()
    val repositories = MutableLiveData<List<Repository>>()
    val error = SingleLiveData<DefaultException>()

    fun getSpace() = R.dimen.space

    fun fetchRepositories() {
        val query = HashMap<String, String>()
        query["q"] = "kotlin"
        query["sort"] = "stars"
        query["per_page"] = "50"

        subscribeSingle(
            observable = useCase.getRepositories(query)
                .doOnSubscribe { showLoader.set(true) }
                .doFinally { showLoader.set(false) },
            success = { repositories.value = it },
            error = { error.postValue(it) }
        )
    }
}