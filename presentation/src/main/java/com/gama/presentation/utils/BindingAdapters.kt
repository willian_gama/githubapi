package com.gama.presentation.utils

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.request.RequestOptions
import com.gama.presentation.R

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("android:show")
    fun View.show(show: Boolean) {
        visibility = if (show) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("android:src_url")
    fun ImageView.srcUrl(url: String?) {
        val options = RequestOptions
            .encodeQualityOf(50)
            .placeholder(R.color.grey)
            .priority(Priority.HIGH)

        Glide.with(context)
            .load(url)
            .apply(options)
            .into(this)
    }

    @JvmStatic
    @BindingAdapter("android:space")
    fun RecyclerView.space(space: Int) {
        val dimen = context.resources.getDimension(space).toInt()
        val decoration = SpaceItemDecoration(dimen)
        addItemDecoration(decoration)
    }

}