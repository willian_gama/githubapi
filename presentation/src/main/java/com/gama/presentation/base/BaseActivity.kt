package com.gama.presentation.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import org.koin.androidx.viewmodel.ext.android.getViewModel
import kotlin.reflect.KClass

abstract class BaseActivity<B : ViewDataBinding, V : BaseViewModel> : AppCompatActivity() {
    protected lateinit var binding: B

    lateinit var viewModel: V

    abstract fun getLayoutRes(): Int

    abstract fun getViewModelByClass(): KClass<V>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, getLayoutRes())
        viewModel = getViewModel(getViewModelByClass())
    }
}