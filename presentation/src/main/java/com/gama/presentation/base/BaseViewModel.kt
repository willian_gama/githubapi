package com.gama.presentation.base

import androidx.lifecycle.ViewModel
import com.gama.domain.exception.DefaultException
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {
    protected fun <T> subscribeSingle(
        observable: Single<T>,
        success: ((T) -> Unit)? = null,
        error: ((DefaultException) -> Unit)? = null
    ): Disposable {
        return observable
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(success, {
                when (it) {
                    is DefaultException -> error?.invoke(it)
                    else -> error?.invoke(DefaultException())
                }
            })
    }
}