package com.gama.presentation.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerViewAdapter<B : ViewDataBinding, V : RecyclerView.ViewHolder> : RecyclerView.Adapter<V>() {
    protected fun dataBinding(parent: ViewGroup, layout: Int): B {
        return DataBindingUtil.inflate(LayoutInflater.from(parent.context), layout, parent, false)
    }
}