package com.gama.data.di

import com.gama.data.base.createWebService
import com.gama.data.di.Properties.SERVER_URL
import com.gama.data.repository.GitHubRepositoryImpl
import com.gama.data.repository.GitHubService
import com.gama.domain.repository.GitHubRepository
import org.koin.dsl.module

val dataModule = module {
    single<GitHubService> { createWebService(getProperty(SERVER_URL)) }

    single<GitHubRepository> { GitHubRepositoryImpl(get()) }
}

object Properties {
    const val SERVER_URL = "SERVER_URL"
}