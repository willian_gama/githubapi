package com.gama.data.response

import com.gama.domain.model.Repository
import com.squareup.moshi.Json

class RepositoryResponse {
    @field:Json(name = "total_count")
    var totalCount: Long? = null

    @field:Json(name = "incomplete_results")
    val incompleteResults: Boolean? = null

    @field:Json(name = "items")
    var items: List<Repository>? = null
}