package com.gama.data.repository

import com.gama.data.response.RepositoryResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface GitHubService {
    @GET("search/repositories")
    fun getRepositories(@QueryMap query: Map<String, String>): Single<RepositoryResponse>
}