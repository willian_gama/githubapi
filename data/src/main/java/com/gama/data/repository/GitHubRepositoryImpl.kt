package com.gama.data.repository

import com.gama.domain.model.Repository
import com.gama.domain.repository.GitHubRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class GitHubRepositoryImpl(private val service: GitHubService) : GitHubRepository {
    override fun getRepositories(query: Map<String, String>): Single<List<Repository>> {
        return service.getRepositories(query)
            .subscribeOn(Schedulers.io())
            .map { it.items }

    }
}