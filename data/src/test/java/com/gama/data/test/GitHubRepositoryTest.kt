package com.gama.data.test

import com.gama.data.base.DataBaseTest
import com.gama.domain.repository.GitHubRepository
import org.junit.Test
import org.koin.test.inject

class GitHubRepositoryTest : DataBaseTest() {
    private val repository: GitHubRepository by inject()

    @Test
    fun `Test getRepositories() success`() {
        val query = HashMap<String, String>().apply {
            this["q"] = "kotlin"
            this["sort"] = "stars"
            this["per_page"] = "50"
        }

        val test = repository.getRepositories(query).test()
        test.assertNoErrors()
        test.awaitTerminalEvent()
        test.assertValue { response -> response.count() <= 50 }
    }
}