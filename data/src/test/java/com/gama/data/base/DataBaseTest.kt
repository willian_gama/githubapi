package com.gama.data.base

import com.gama.data.di.dataModule
import org.junit.After
import org.junit.Before
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest

abstract class DataBaseTest : KoinTest {
    @Before
    fun `start test`() {
        startKoin {
            printLogger()
            fileProperties()
            modules(listOf(dataModule))
        }
    }

    @After
    fun `finish test`() = stopKoin()
}