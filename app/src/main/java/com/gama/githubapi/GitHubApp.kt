package com.gama.githubapi

import android.app.Application
import com.gama.data.di.dataModule
import com.gama.domain.di.domainModule
import com.gama.presentation.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric

class GitHubApp : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@GitHubApp)
            androidLogger(Level.DEBUG)
            androidFileProperties()
            modules(listOf(dataModule, domainModule, presentationModule))
        }

        Fabric.with(this, Crashlytics())
    }
}