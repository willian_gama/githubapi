package com.gama.githubapi

import androidx.lifecycle.Observer
import com.gama.domain.model.Repository
import com.gama.presentation.ui.activity.RepositoryActivity
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.android.controller.ActivityController

@RunWith(RobolectricTestRunner::class)
class RepositoryActivityTest : KoinTest {
    private val observerRepository: Observer<List<Repository>> = mock()

    private lateinit var controller: ActivityController<RepositoryActivity>

    @Before
    fun `start test`() {
        controller = Robolectric.buildActivity(RepositoryActivity::class.java)
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
    }

    @After
    fun `finish test`() {
        stopKoin()
        RxJavaPlugins.reset()
        controller.destroy()
    }

    @Test
    fun `Test fetchPopularMovies() when activity is created`() {
        // PREPARE
        val activity = controller.get()

        // ACTION
        controller.create()

        activity.viewModel.repositories.observeForever(observerRepository)

        // TEST
        verify(observerRepository).onChanged(any())
    }
}