package com.gama.githubapi

import com.gama.data.di.dataModule
import com.gama.domain.di.domainModule
import com.gama.presentation.di.presentationModule
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.dsl.koinApplication
import org.koin.test.KoinTest

class ModulesTest : KoinTest {
    private val ctx by lazy { GitHubApp() }

    @Test
    fun `test modules`() {
        koinApplication {
            androidContext(ctx)
            androidFileProperties()
            modules(listOf(dataModule, domainModule, presentationModule))
        }
    }
}