package com.gama.domain.repository

import com.gama.domain.model.Repository
import io.reactivex.Single

interface GitHubRepository {
    fun getRepositories(query: Map<String, String>): Single<List<Repository>>
}