package com.gama.domain.model

import com.squareup.moshi.Json

class Owner {
    @field:Json(name = "id")
    val id: String? = null

    @field:Json(name = "login")
    val login: String? = null

    @field:Json(name = "avatar_url")
    val avatarUrl: String? = null
}