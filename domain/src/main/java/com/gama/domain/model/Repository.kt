package com.gama.domain.model

import com.squareup.moshi.Json

class Repository {
    @field:Json(name = "id")
    val id: Long? = null

    @field:Json(name = "name")
    val name: String? = null

    @field:Json(name = "description")
    val description: String? = null

    @field:Json(name = "forks_count")
    val forksCount: Long? = null

    @field:Json(name = "stargazers_count")
    val stargazersCount: Long? = null

    @field:Json(name = "pulls_url")
    val pullsUrl: String? = null

    @field:Json(name = "owner")
    val owner: Owner? = null
}