package com.gama.domain.usecase

import com.gama.domain.model.Repository
import com.gama.domain.repository.GitHubRepository
import io.reactivex.Single

class RepositoryUseCase(private val repository: GitHubRepository) {
    fun getRepositories(query: Map<String, String>): Single<List<Repository>> {
        return repository.getRepositories(query)
    }
}