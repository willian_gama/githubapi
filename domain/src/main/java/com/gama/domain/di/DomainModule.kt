package com.gama.domain.di

import com.gama.domain.usecase.RepositoryUseCase
import org.koin.dsl.module

val domainModule = module {
    factory { RepositoryUseCase(get()) }
}