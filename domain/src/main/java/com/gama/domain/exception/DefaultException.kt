package com.gama.domain.exception

import com.squareup.moshi.Json

class DefaultException : Exception() {
    @field:Json(name = "message")
    override val message: String = "Unknown error"

    @field:Json(name = "documentation_url")
    val documentationUrl: String? = null

    @field:Json(name = "errors")
    val errors: List<Errors>? = null
}

class Errors {
    @field:Json(name = "resource")
    val resource: String? = null

    @field:Json(name = "field")
    val field: String? = null

    @field:Json(name = "code")
    val code: String? = null
}