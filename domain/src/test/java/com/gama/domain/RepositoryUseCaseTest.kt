package com.gama.domain

import com.gama.domain.repository.GitHubRepository
import com.gama.domain.usecase.RepositoryUseCase
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.anyMap
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class RepositoryUseCaseTest {
    @Mock
    lateinit var repository: GitHubRepository

    @InjectMocks
    private lateinit var useCase: RepositoryUseCase

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `Test getRepositories`() {
        // ACTION
        useCase.getRepositories(anyMap())

        // TEST
        verify(repository).getRepositories(anyMap())
    }
}